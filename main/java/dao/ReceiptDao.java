/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author USER
 */
import com.pin.storeproject1.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author USER
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "INSERT INTO product (name,price)VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setDouble(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO receipt (customer_id,user_id,total)VALUES (?,?,?.?)";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
        }

        db.close();
        return -1;
    }

    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.id as id,\n"
                    + " created,\n"
                    + " customer_id,\n"
                    + " c.name as customer_name,\n"
                    + " c.tel as customer_tel,\n"
                    + " user_id,\n"
                    + " u.name as user_name,\n"
                    + " u.tel as user_tel,\n"
                    + " total\n"
                    + " FROM receipt r,customer c,user u\n"
                    + " WHERE r.customer_id=c.id AND r.user_id=u.id;"
                    + " ORDER BY created DESC:";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(result.getString("CREATION_TIME"));
                int customerId = result.getInt("custemer_id");
                String custemerName = result.getString("custemer_name");
                String custemerTel = result.getString("custemer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, custemerName, custemerTel));
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt!!" + ex.getMessage());
        }

        db.close();
        return list;
    }

    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.id as id,\n"
                    + " created,\n"
                    + " customer_id,\n"
                    + " c.name as customer_name,\n"
                    + " c.tel as customer_tel,\n"
                    + " user_id,\n"
                    + " u.name as user_name,\n"
                    + " u.tel as user_tel,\n"
                    + " total\n"
                    + " FROM receipt r,customer c,user u\n"
                    + " WHERE r.id=? AND r.customer_id=c.id AND r.user_id=u.id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(result.getString("CREATION_TIME"));
                int customerId = result.getInt("custemer_id");
                String custemerName = result.getString("custemer_name");
                String custemerTel = result.getString("custemer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(rid, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, custemerName, custemerTel));
                getReceiptDetail(conn, stmt, id, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt id" + id + "!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt!!" + ex.getMessage());
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, PreparedStatement stmt, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + " receiot,\n"
                + " product_id,\n"
                + " p.name as product_name,\n"
                + " p.price as product_price,\n"
                + " rd.price as prcice,\n"
                + " amount\n"
                + " FROM receipt r,customer c,user u\n"
                + " WHERE r.id=? AND r.customer_id=c.id AND r.user_id=u.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmt.setInt(1, id);
        ResultSet resultDetail = stmt.executeQuery();
        while (resultDetail.next()) {
            int receiveId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productprice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productprice);
            receipt.addReceiptDetail(receiveId, product, amount, price);
            
        }
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete select all receipt!!" + ex.getMessage());
        }

        db.close();
        return row;
    }

    public int update(Product object) {
        //Connection conn = null;
        // Database db = Database.getInstance();
        // conn = db.getConnection();
        //int row = 0;
        // try {
        //   String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?;";
        //  PreparedStatement stmt = conn.prepareStatement(sql);
        //  stmt.setString(1, object.getName());
        //  stmt.setDouble(2, object.getPrice());
        //  stmt.setInt(3, object.getId());
        // row = stmt.executeUpdate();
        //} catch (SQLException ex) {
        // Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        // }

        // db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "chayen", 30);
        Product p2 = new Product(2, "Americano", 40);
        User seller = new User("Yanisa Srisompong", "88888888");
        Customer customer = new Customer(1, "Yaya", "99999999");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id=" + dao.add(receipt));
        System.out.println("Receipt after add: " + receipt);
        System.out.println("Get all:" + dao.getAll());

        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New Receipt:" + newReceipt);
        
        dao.delete(receipt.getId());
    }

    @Override
    public int update(Receipt object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
